import java.util.Scanner;

public class gameRunner {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Do you wish to encrypt or decrypt a message?");
        String answer = in.nextLine();

        if(answer.equals("encrypt")){
            encrypt();
        }
        if (answer.equals("decrypt")){
            decrypt();
        }

    }

    public static  void encrypt(){

        String message, encryptedMessage = "";
        int key;
        char ch;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a message: ");
        message = sc.nextLine();

        System.out.println("Enter key(1-52): ");
        key = sc.nextInt();

        for(int i = 0; i < message.length(); ++i){
            ch = message.charAt(i);

            if(ch >= 'a' && ch <= 'z'){
                ch = (char)(ch + key);

                if(ch > 'z'){
                    ch = (char)(ch - 'z' + 'a' - 1);
                }

                encryptedMessage += ch;
            }
            else if(ch >= 'A' && ch <= 'Z'){
                ch = (char)(ch + key);

                if(ch > 'Z'){
                    ch = (char)(ch - 'Z' + 'A' - 1);
                }

                encryptedMessage += ch;
            }
            else {
                encryptedMessage += ch;
            }
        }

        System.out.println("Encrypted Message = " + encryptedMessage);
    }
    public static void decrypt(){

        String message, decryptedMessage = "";
        int key;
        char ch;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a message: ");
        message = sc.nextLine();

        System.out.println("Enter key(1-52): ");
        key = sc.nextInt();

        for(int i = 0; i < message.length(); ++i){
            ch = message.charAt(i);

            if(ch >= 'a' && ch <= 'z'){
                ch = (char)(ch - key);

                if(ch < 'a'){
                    ch = (char)(ch + 'z' - 'a' + 1);
                }

                decryptedMessage += ch;
            }
            else if(ch >= 'A' && ch <= 'Z'){
                ch = (char)(ch - key);

                if(ch < 'A'){
                    ch = (char)(ch + 'Z' - 'A' + 1);
                }

                decryptedMessage += ch;
            }
            else {
                decryptedMessage += ch;
            }
        }

        System.out.println("Decrypted Message = " + decryptedMessage);
    }
}
